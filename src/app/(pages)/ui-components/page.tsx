'use client';

import React from 'react';
import Container from '@mui/material/Container';
import { Button, Divider, TextField } from '@mui/material';
import IconButton from '@mui/material/IconButton';

import { DarkAccordion } from './accordion/accordion';
import { LightAccordion } from './accordion/accordion';
import Icon from './icon/Icon';

const Uicomponents = () => {
  return (
    <Container
      fixed
      sx={{
        display: 'flex',
        flexDirection: 'column',
      }}
    >
      <div className='flex flex-col'>
        <h1 className='t-46-64 my-6 font-inter font-bold'>HEADINGS</h1>

        <h2 className='t-46-64 mb-4 font-frutiger font-bold'>
          Frutiger spans from 46 to 64 with 700 bold
        </h2>
        <h2 className='t-33-46 mb-4 font-frutiger font-bold'>
          Frutiger spans from 33 to 46
        </h2>
        <h2 className='t-30-46 mb-4 font-frutiger font-bold'>
          Frutiger spans from 30 to 46
        </h2>
        <h2 className='t-25-36 mb-4 font-frutiger font-bold'>
          Frutiger spans from 25 to 36
        </h2>
        <h2 className='t-22-28 mb-4 font-frutiger font-bold'>
          Frutiger spans from 22 to 28
        </h2>
        <h2 className='t-21-18 mb-4 font-frutiger font-bold'>
          Frutiger spans from 21 to 18
        </h2>
        <h2 className='t-16-17 mb-4 font-frutiger font-semibold'>
          Frutiger spans from 16 to 17
        </h2>
        <h2 className='t-14 mb-4 font-frutiger font-bold'>Frutiger 14</h2>
        <h2 className='t-12 mb-4 font-frutiger font-normal uppercase'>
          Frutiger 12
        </h2>
        <h2 className='t-13 mb-4 font-frutiger font-semibold uppercase'>
          Frutiger 13
        </h2>

        <h2 className='t-18-19 mb-4 font-semibold'>Inter 18-19</h2>

        <h2 className='t-16-17 mb-4 font-semibold'>Inter 16-17</h2>

        <h2 className='t-14-15 mb-4 font-semibold'>Inter-14-15</h2>

        <h2 className='t-13 mb-4 font-semibold'>Inter-13</h2>

        <h2 className='t-25-36 my-6 font-semibold'>Primary Button</h2>

        <div className='md:d-3/4 grid w-full grid-cols-2 gap-4 lg:w-1/2'>
          <Button
            variant='contained'
            color='primary'
            disableFocusRipple
            sx={{ marginBottom: 4 }}
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              primary
            </span>
          </Button>

          <Button
            variant='contained'
            color='primary'
            disableFocusRipple
            disabled
            sx={{ marginBottom: 4 }}
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              primary disabled
            </span>
          </Button>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>Secondary button</h2>

        <div className='md:d-3/4 grid w-full grid-cols-2 gap-4 lg:w-1/2'>
          <Button
            variant='contained'
            color='secondary'
            disableFocusRipple
            sx={{ marginBottom: 4 }}
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              secondary
            </span>
          </Button>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>outline button</h2>

        <div className='md:d-3/4 grid w-full grid-cols-2 gap-4 lg:w-1/2'>
          <Button
            variant='outlined'
            color='secondary'
            disableFocusRipple
            sx={{ marginBottom: 4 }}
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              outline
            </span>
          </Button>

          <Button
            variant='outlined'
            color='secondary'
            disableFocusRipple
            className='icon-btn-movement'
            sx={{ marginBottom: 4 }}
            endIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              outline icon
            </span>
          </Button>

          <Button
            variant='outlined'
            color='secondary'
            disableFocusRipple
            className='icon-btn-movement'
            sx={{ marginBottom: 4 }}
            startIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              outline icon
            </span>
          </Button>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>Icon button</h2>

        <div className='md:d-3/4 grid w-full grid-cols-2 gap-4 lg:w-1/2'>
          <Button
            variant='contained'
            color='primary'
            disableFocusRipple
            className='icon-btn-movement'
            sx={{ marginBottom: 4 }}
            endIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              icon
            </span>
          </Button>

          <Button
            variant='contained'
            color='primary'
            disableFocusRipple
            className='icon-btn-movement'
            sx={{ marginBottom: 4 }}
            startIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              icon
            </span>
          </Button>

          <IconButton
            color='primary'
            disableFocusRipple
            sx={{ marginBottom: 4 }}
            className='icon-24'
          >
            <Icon
              iconName='rightArrow'
              className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
            ></Icon>
          </IconButton>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>Form control group</h2>

        <div className='input-group-cta mb-4 flex items-center px-4 py-2'>
          <TextField
            required
            id='outlined-required'
            label='Required'
            defaultValue=''
            variant='filled'
            className='filled-borderless-field'
            sx={{ marginBottom: 0 }}
          />
          <Divider sx={{ height: 48, m: 0.5 }} orientation='vertical' />
          <TextField
            required
            id='outlined-required'
            label='Required'
            defaultValue=''
            variant='filled'
            className='filled-borderless-field'
            sx={{ marginBottom: 0 }}
          />
          <Button
            variant='contained'
            color='primary'
            disableFocusRipple
            className='icon-btn-movement md:ml-1'
            sx={{ marginBottom: 0, marginLeft: 1 }}
            endIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
          >
            <span className='t-15 font-inter font-medium leading-snug'>
              icon
            </span>
          </Button>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>Borderless Links</h2>

        <div className='md:d-3/4 grid w-full grid-cols-2 gap-4 lg:w-1/2'>
          <Button
            variant='text'
            color='secondary'
            disableFocusRipple
            sx={{ padding: 0 }}
          >
            <span className='t-15 font-medium normal-case leading-normal'>
              link text
            </span>
          </Button>

          <Button variant='text' color='secondary' disableFocusRipple>
            <span className='t-15 font-medium normal-case leading-normal'>
              Borderless
            </span>
          </Button>

          <Button variant='text' color='secondary' disableFocusRipple>
            <span className='t-15 font-medium leading-normal'>upper</span>
          </Button>

          <Button
            variant='text'
            className='icon-btn-movement md:ml-1'
            endIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              ></Icon>
            }
            color='secondary'
            disableFocusRipple
          >
            <span className='t-15 font-medium normal-case leading-normal'>
              Borderless with icon
            </span>
          </Button>

          <Button
            variant='text'
            className='icon-btn-movement md:ml-1'
            startIcon={
              <Icon
                iconName='rightArrow'
                className='svg-icon icon-24 flex items-center justify-center'
              ></Icon>
            }
            color='secondary'
            disableFocusRipple
          >
            <span className='t-15 font-medium normal-case leading-normal'>
              Borderless with icon
            </span>
          </Button>
        </div>

        <h2 className='t-25-36 my-6 font-semibold'>Accordion</h2>

        <div className='accordion'>
          <DarkAccordion />
          <DarkAccordion />
          <div className='mt-6'>
            <LightAccordion />
          </div>
        </div>
      </div>
    </Container>
  );
};

export default Uicomponents;
