import React, { useState } from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@emotion/react';
import Icon from '../icon/Icon';

const darkAccordionTheme = createTheme({
  components: {
    MuiAccordion: {
      styleOverrides: {
        root: {
          boxShadow: 'none',
        },
      },
    },
    MuiAccordionSummary: {
      styleOverrides: {
        root: {
          backgroundColor: '#f6f7f7',
          boxShadow: 'none',
          '&.Mui-expanded': {
            marginBottom: 0,
            background: '#f6f7f7',
          },
          '&.MuiAccordionSummary-root': {
            background: '#f6f7f7',
          },
          '&.MuiPaper-root': {
            boxShadow: 'none',
          },
        },
        content: {
          '&.Mui-expanded': {
            marginBottom: 1,
          },
        },
      },
    },
    MuiAccordionDetails: {
      styleOverrides: {
        root: {
          backgroundColor: '#f6f7f7',
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          '&.MuiAccordion-root::before': {
            backgroundColor: 'transparent',
          },
        },
      },
    },
  },
});

export function LightAccordion() {
  const [isExpanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(!isExpanded);
  };

  return (
    <>
      <Accordion
        onClick={() => {
          handleToggle();
        }}
        className='mb-1'
      >
        <AccordionSummary
          className='accordion-summary'
          expandIcon={
            <>
              {
                <Icon
                  iconName={isExpanded ? 'minus' : 'plus'}
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                />
              }
            </>
          }
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <div className='flex items-center'>
            <div className='accordion-icon mr-2 md:mr-4'>
              <Icon
                iconName='person'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              />
            </div>
            <h6 className='t-16-17 font-frutiger font-bold'>Light accordion</h6>
          </div>
        </AccordionSummary>
        <AccordionDetails className='accordion-details'>
          <div className='-mt-4'>
            <p className='t-14-15'>
              We provide long-term nursing care to individuals, including those
              needing tracheostomy and ventilator care. BAYADAbility Rehab
              Solutions delivers specialized care for adults with ALS, MS, or
              catastrophic diagnoses such as spinal cord and traumatic brain
              injuries.
            </p>
          </div>
        </AccordionDetails>
      </Accordion>
    </>
  );
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const DarkAccordion = (accordionDatas: any) => {
  const [isExpanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(!isExpanded);
  };

  return (
    <ThemeProvider theme={darkAccordionTheme}>
      <Accordion
        onClick={() => {
          handleToggle();
        }}
        className='mb-1'
      >
        <AccordionSummary
          className='accordion-summary'
          expandIcon={
            <>
              {
                <Icon
                  iconName={isExpanded ? 'minus' : 'plus'}
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                />
              }
            </>
          }
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <div className='flex items-center'>
            <div className='accordion-icon mr-2 md:mr-4'>
              <Icon
                iconName='person'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              />
            </div>
            {accordionDatas && accordionDatas?.accordionData && (
              <h6 className='t-16-17 font-frutiger font-bold'>
                {accordionDatas.accordionData.title}
              </h6>
            )}
          </div>
        </AccordionSummary>
        <AccordionDetails className='accordion-details position-relative'>
          <div className='pl-5'>
            {accordionDatas && accordionDatas?.accordionData && (
              <p className='t-14-15 pl-5'>
                {accordionDatas.accordionData.content}
              </p>
            )}
          </div>
        </AccordionDetails>
      </Accordion>
    </ThemeProvider>
  );
};
