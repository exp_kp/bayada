'use client';

import HomePage from '@/app/components/Pages/Home/Index';
import React from 'react';
import { useSearchParams } from 'next/navigation';
import { ContentfulLivePreviewProvider } from '@contentful/live-preview/react';

const PreviewPage = () => {
  const searchParams = useSearchParams();
  const sysId = searchParams.get('sysId');
  const space_id = searchParams.get('space_id');
  const delivery_token = searchParams.get('delivery_token');
  const preview_token = searchParams.get('preview_token');

  console.log(sysId, ' ::: sysId');
  console.log(space_id, ' ::: space_id');
  console.log(delivery_token, ' ::: delivery_token');
  console.log(preview_token, ' ::: preview_token');

  return (
    <>
      <ContentfulLivePreviewProvider
        locale='en-US'
        enableLiveUpdates
        enableInspectorMode
      >
        <HomePage
          isPreview={true}
          previewSecrets={{ sysId, space_id, delivery_token, preview_token }}
        />
      </ContentfulLivePreviewProvider>
    </>
  );
};

export default PreviewPage;
