import HomePage from '@/app/components/Pages/Home/Index';
import React from 'react';
import { getEntries } from '../services/deliveryService';

async function getData() {
  const res = await getEntries({
    content_type: 'page',
  });
  const items = res?.items[0];
  return items;
}

const Page = async () => {
  console.log('Page is rendered');
  const data = await getData();
  console.log(data, 'data');
  return (
    <>
      <HomePage data={data} />
    </>
  );
};

export default Page;
