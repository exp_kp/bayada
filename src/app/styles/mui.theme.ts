import { Breakpoint, createTheme } from '@mui/material/styles';
import style from '../assets/scss/abstracts/_breakpoints.module.scss';
import colors from '../assets/scss/abstracts/_palette.module.scss';

declare module '@mui/material/styles' {
  interface BreakpointOverrides {
    xs: true;
    sm: true;
    md: true;
    lg: true;
    xl: true;
    xxl: true;
    xxxl: true;
  }
}

const styleBreakPoints: { [key in Breakpoint]: number } = {
  xs: 0,
  sm: 0,
  md: 0,
  lg: 0,
  xl: 0,
  xxl: 0,
  xxxl: 0,
};

if (style) {
  for (const key in style) {
    const breakPoint = key as Breakpoint;
    styleBreakPoints[breakPoint] = Number(style[key]);
  }
}

const theme = createTheme({
  palette: {
    primary: {
      main: colors ? colors['ba-primary-red'] : '#CE0E2D',
    },
    secondary: {
      main: colors ? colors['ba-primary-black'] : '#232323',
    },
  },
  components: {
    MuiToolbar: {
      styleOverrides: {
        root: {
          backgroundColor: 'transparent',
          minHeight: '44px !important',
          '&.bg-white': {
            background: 'var(--white)',
          },
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          boxShadow: 'none',
          backgroundColor: 'transparent',
          '&.MuiAppBar-colorPrimary': {
            color: 'var(--ba-primary-black)',
          },
        },
      },
    },
    MuiContainer: {
      defaultProps: {
        maxWidth: false,
      },
    },
    MuiButton: {
      variants: [
        {
          props: {
            variant: 'contained',
            color: 'primary',
          },
          style: {
            transition: 'all 0.3s ease-in-out',
            background:
              'linear-gradient(96deg, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%)',
            '&:hover': {
              background:
                'linear-gradient(96deg, var(--ba-primary-600) 0%, var(--ba-primary-700) 19.27%, var(--ba-primary-700) 66.15%, var(--ba-primary-800) 100%)',
            },
            '&:focus': {
              background: colors ? colors['ba-btn-focus-bg'] : '#AE0420',
            },
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            '&.top-navbar-actions': {
              background: 'rgba(35, 35, 35, 0.2)',
            },
          },
        },
        {
          props: { variant: 'contained', color: 'primary', disabled: true },
          style: {
            transition: 'all 0.3s ease-in-out',
            background: colors ? colors['ba-btn-disabled'] : '#C7C7C7',
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            pointerEvents: 'none',
          },
        },
        {
          props: {
            variant: 'contained',
            color: 'secondary',
          },
          style: {
            transition: 'all 0.3s ease-in-out',
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            '&:hover': {
              background: colors ? colors['ba-primary-black'] : '#232323',
              color: '#ffffff',
            },
          },
        },
        {
          props: {
            variant: 'outlined',
            color: 'secondary',
          },
          style: {
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            transition: 'all 0.3s ease-in-out',
            '&:hover': {
              color: '#ffffff',
              background: colors ? colors['ba-primary-black'] : '#232323',
            },
            '&:focus': {
              background: '#000000',
              color: '#ffffff',
            },
          },
        },
        {
          props: {
            variant: 'text',
            color: 'primary',
          },
          style: {
            textTransform: 'none',
            '&.top-navbar-actions': {
              background: 'rgba(35, 35, 35, 0.2)',
              transition: 'all 0.2s ease-in-out',
              marginLeft: 8,
              '&:first-child': {
                marginLeft: 0,
              },
              '&:hover': {
                background: 'rgba(35, 35, 35, 0.9)',
              },
            },
          },
        },
      ],
      styleOverrides: {
        root: {
          '&.bg-white': {
            background: 'var(--white)',
          },
          '&.hero-search': {
            padding: '16px',
          },
        },
      },
    },
    MuiIconButton: {
      variants: [
        {
          props: { color: 'primary' },
          style: {
            transition: 'background 0.3s ease-in-out',
            background:
              'linear-gradient(96deg, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%)',
            '&:hover': {
              background:
                'linear-gradient(96deg, var(--ba-primary-600) 0%, var(--ba-primary-700) 19.27%, var(--ba-primary-700) 66.15%, var(--ba-primary-800) 100%)',
            },
            '&:focus': {
              background: colors ? colors['ba-btn-focus-bg'] : '#AE0420',
            },
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
          },
        },
      ],
    },
    MuiTextField: {
      variants: [
        {
          props: {},
          style: {},
        },
      ],
      styleOverrides: {
        root: {
          '&:hover': {
            backgroundColor: colors ? colors['ba-field-hover-bg'] : '#F3F3F3',
          },
          '&:focus': {
            backgroundColor: colors ? colors['ba-field-focus-bg'] : '#EAEAEA',
          },
          backgroundColor: '#ffffff',
          underline: {
            '&:hover:not($disabled):before': {
              borderBottom: 'none', // Remove the bottom border on hover
            },
          },
        },
      },
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          '&.app-search-form-control': {
            width: 'clamp(40%, 100px, 100%)',
            '&:hover': {
              background: '#F3F3F3',
            },
            '&:focus': {
              background: '#EAEAEA',
            },
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          '&.search-input-label': {
            color: 'var(--ba-gray-900)',
            left: 12,
            top: '-8px',
            '&.Mui-focused': {
              top: '0px',
            },
            '&.MuiFormLabel-filled': {
              top: '0px',
            },
          },
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        root: {
          '&.app-search-form-select': {
            marginTop: 0,
            paddingTop: 16,
            paddingLeft: 12,

            '&.Mui-focused': {
              marginTop: 16,
              paddingTop: 0,
            },
            '&:before': {
              borderColor: 'transparent',
            },
            '&:not(.Mui-disabled, .Mui-error):before': {
              borderColor: 'transparent',
            },
          },
        },
      },
    },
  },
  typography: {
    fontFamily: ["'Inter', sans-serif", "'frutiger', sans-serif"].join(','),
    fontSize: 16,
  },
  breakpoints: {
    values: styleBreakPoints,
  },
});

export default theme;
