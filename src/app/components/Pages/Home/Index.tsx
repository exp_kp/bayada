/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';

import React, { useEffect, useState } from 'react';
import { Entry, EntrySkeletonType } from 'contentful';
import { getEntries } from '@/app/services/deliveryService';
import { Alert, AlertTitle } from '@mui/material';
import { useContentfulLiveUpdates } from '@contentful/live-preview/react';
import Section from '../../common/Section/Section';

type Home = {
  data: Entry<EntrySkeletonType, undefined, string> | undefined;
};

const HomePage: React.FC<Home> = ({ data }) => {
  const [state, setState] = useState<
    Entry<EntrySkeletonType, undefined, string> | undefined
  >(data);
  const updatedState = useContentfulLiveUpdates(state);

  // useEffect(() => {
  //   getHomePage();

  //   return () => {
  //     // second
  //   };
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  // useEffect(() => {
  //   setPreviewState({ isPreview, previewSecrets });
  // }, [isPreview, previewSecrets]);

  // const getHomePage = async () => {
  //   if (previewState?.isPreview) {
  //     const res = await getEntries({
  //       content_type: 'page',
  //     });
  //     const items = res?.items[0];
  //     console.log(items, 'getHomePage items');
  //     setState(items);
  //   } else {
  //     const res = await getEntries({
  //       content_type: 'page',
  //     });
  //     const items = res?.items[0];
  //     console.log(items, 'getHomePage items');
  //     setState(items);
  //   }
  // };

  const { sections } = updatedState?.fields ?? {};
  console.log(sections, 'sections');

  return (
    <div className='page'>
      {(sections as Array<unknown>)?.map((section: any) => {
        return <Section data={section} key={section?.sys?.id} />;
      })}
      <Alert
        severity='warning'
        sx={{
          position: 'fixed',
          bottom: '0',
          left: '0',
          width: '100%,',
          zIndex: 11,
        }}
      >
        <AlertTitle>Note</AlertTitle>
        This pre-sprint build (V 0.1 beta) is exclusively for testing Contentful
        API payloads, Live editing, Inspect Element SDK, front-end mapping
        methods and architectural flow. Its primary purpose is to collect logs
        for future reference. Please note that this build is not intended for
        validating design, style, or layouts.
      </Alert>
    </div>
  );
};

export default HomePage;
