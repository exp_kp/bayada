/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box, Container, Grid } from '@mui/material';

const ListOfFacts = (props: any) => {
  const { list } = props;
  const listOfFacts = list;

  return (
    <div className='my-10 flex items-center justify-center'>
      <Container fixed className='mx-auto'>
        <div className='xxl:w-1/2 mx-auto flex w-full items-center justify-center lg:w-3/4'>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 1, sm: 8, md: 12 }}
          >
            {listOfFacts?.textGroup?.map((item: any, index: number) => (
              <Grid item xs={2} sm={4} md={4} key={index}>
                <div className='fact-list-card flex h-full flex-col items-center justify-start rounded-md px-2 py-6 text-center md:py-8'>
                  <h3 className='t-13 mt-2'>{item?.fields?.title}</h3>
                  <h3 className='t-22-28 mb-4 font-frutiger font-bold'>
                    {item?.fields?.description}
                  </h3>
                </div>
              </Grid>
            ))}
          </Grid>
        </div>
      </Container>
    </div>
  );
};
export default ListOfFacts;
