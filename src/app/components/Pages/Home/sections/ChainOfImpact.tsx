/* eslint-disable @typescript-eslint/no-explicit-any */
import { Container, Button } from '@mui/material';
import Icon from '../../../../(pages)/ui-components/icon/Icon';
import ContentfulImage from '@/app/components/common/ContentfulImage';
import { HomeStepper } from './HomeStepper';

// import ContentfulImage from '@/app/components/common/ContentfulImage';
interface ChainOfImpactProps {
  title: string;
  description: string;
  paragraph: string;
  image: string;
  imageText: string;
  width?: number;
  height?: number;
}
const ChainOfImpact = ({ response }: any) => {
  const chainOfImpactDetails: ChainOfImpactProps = {
    title: response?.TextGroup?.fields?.title,
    description: response?.TextGroup?.fields?.description,
    paragraph: response?.TextGroup?.fields?.paragraph,
    image: response?.Media?.fields?.file?.url,
    imageText: response?.Media?.fields?.title,
    ...response?.Media?.fields?.file?.details?.image,
  };
  console.log(chainOfImpactDetails, 'chainOfImpactDetails');

  return (
    <>
      <Container fixed sx={{ py: 16 }}>
        <div className='flex flex-col-reverse gap-2 md:flex-row'>
          <div className='flex w-full flex-col md:w-1/2'>
            <div className='flex flex-col justify-start md:mr-11'>
              <h2 className='t-46-64 mb-4 font-frutiger font-bold'>
                {chainOfImpactDetails?.title}
              </h2>
              <h2 className='t-22-28 mb-4 font-frutiger font-bold'>
                {chainOfImpactDetails?.description}
              </h2>
              <h2
                className='t-14 mb-4 font-frutiger font-normal'
                style={{ color: '#606060', fontSize: 16 }}
              >
                {chainOfImpactDetails?.paragraph}
              </h2>
              <Button
                variant='contained'
                color='primary'
                disableFocusRipple
                className='icon-btn-movement !mr-auto rounded-full'
                sx={{ marginBottom: 0 }}
                endIcon={
                  <Icon
                    iconName='rightArrow'
                    className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                  ></Icon>
                }
              >
                <span className='t-15 font-inter font-medium leading-snug'>
                  Learn how it works
                </span>
              </Button>
            </div>
          </div>
          <div className='mb-8 flex w-full flex-col justify-start md:mb-0 md:w-1/2 md:items-end'>
            <ContentfulImage
              src={chainOfImpactDetails?.image}
              alt={chainOfImpactDetails?.imageText}
              width={chainOfImpactDetails?.width}
              height={chainOfImpactDetails?.height}
            />
          </div>
        </div>
      </Container>
      {/* <div className='our-client-section'>
        <HomeStepper />
      </div> */}
    </>
  );
};
export default ChainOfImpact;
