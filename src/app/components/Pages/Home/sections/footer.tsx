/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
'use client';

import { Button, Container, TextField } from '@mui/material';
import './footer.scss';
import { client } from '@/app/config/client';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import React, { useEffect, useState } from 'react';
import ContentfulImage from '@/app/components/common/ContentfulImage';

const getFooterContent = async () => {
  try {
    const res = await client?.getEntries({
      content_type: 'footerSection',
      include: 10,
    });

    return res;
  } catch (error) {
    console.log(error, 'FooterPage');
  }
};

const Footer = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [footerContent, setfooterContent] = useState<any>();

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    getFooterContent().then((res: any) => {
      setfooterContent(res.items[0].fields);
    });

    return () => {};
  }, []);

  return (
    <div className='footer-container'>
      <Container fixed className='relative mx-auto'>
        <div className='foot-main'>
          <div className='footer-left'>
            <div className='footer-group'>
              <img
                className='logo'
                // src={footerContent?.logo.fields.image.fields.asset.fields.file.url}
                src='/temp/bayada-color-logo.png'
                alt='Bayada Logo'
              />

              <div className='footer-icons'>
                {// eslint-disable-next-line @typescript-eslint/no-explicit-any
                footerContent?.linkGroup.map((mediaImg: any) => (
                  <ContentfulImage
                    key={mediaImg.sys.id}
                    src={mediaImg.fields.image.fields.asset.fields.file.url}
                    alt={mediaImg.fields.internalName}
                  />
                ))}
              </div>
            </div>

            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            footerContent?.textGroup.map((text: any) => (
              <h2
                key={text.sys.id}
                className='t-21-18 mb-4 w-60 font-frutiger font-bold'
              >
                {text.fields.title}
              </h2>
            ))}
            <div className='input-group-cta mb-4 flex items-center bg-white px-2 py-1'>
              <TextField
                required
                id='outlined-required'
                label='Enter Email Adress'
                defaultValue=''
                variant='filled'
                className='filled-borderless-field search'
                sx={{ marginBottom: 0 }}
              />
              <Button
                variant='contained'
                color='secondary'
                disableFocusRipple
                className='icon-btn-movement md:ml-1'
                sx={{ marginBottom: 0, marginLeft: 1 }}
              >
                <Icon
                  iconName='rightArrow'
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                ></Icon>
              </Button>
            </div>
          </div>
          <div className='footer-service'>
            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            footerContent?.footerMenuGroup.map((menuItem: any) =>
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              menuItem.fields.menuItems.map((data: any, index: number) => (
                <div key={index} className='service-links'>
                  <h2 className='t-14-15 mb-4 font-semibold'>
                    {data.fields.name}
                  </h2>
                  <ul>
                    {
                      // eslint-disable-next-line @typescript-eslint/no-explicit-any
                      data.fields.menuItems.map((listItem: any, i: number) => (
                        <li key={i}>
                          <a href='#'>{listItem.fields.name}</a>
                        </li>
                      ))
                    }
                  </ul>
                </div>
              ))
            )}
          </div>
          <div className='footer-group'>
            <img
              className='logo'
              // src={footerContent?.logo.fields.image.fields.asset.fields.file.url}
              src='/temp/bayada-color-logo.png'
              alt='Bayada Logo'
            />

            <div className='footer-icons'>
              {// eslint-disable-next-line @typescript-eslint/no-explicit-any
              footerContent?.linkGroup.map((mediaImg: any) => (
                <ContentfulImage
                  width={32}
                  height={32}
                  key={mediaImg.sys.id}
                  src={mediaImg.fields.image.fields.asset.fields.file.url}
                  alt={mediaImg.fields.internalName}
                />
              ))}
            </div>
          </div>
        </div>
      </Container>

      <div className='footer-bottom'>
        <Container fixed className='relative mx-auto'>
          <div className='footer-bottom-content'>
            <img src='/temp/chap-logo.png' alt='chap' />
            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            footerContent?.textGroup.map((text: any, index: number) => (
              <p key={index}>{text.fields.description}</p>
            ))}
          </div>
        </Container>
      </div>
    </div>
  );
};

export default Footer;
