/* eslint-disable @next/next/no-img-element */
import React from 'react';
import './SocialMediaCard.scss';
import facebook from './assets/facebook.png';
import instagram from './assets/instagram.png';
import youtube from './assets/youtube.png';
import bayada from './assets/bayada.png';
import { Stack } from '@mui/material';
import Icon from '@/app/(pages)/ui-components/icon/Icon';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function SocialMediaCard(props: any) {
  const { data } = props;

  const getSocialMediaIcon = () => {
    if (data?.socialMediaIcon == 'facebook') {
      return facebook.src;
    } else if (data?.socialMediaIcon == 'instagram') {
      return instagram.src;
    } else if (data?.socialMediaIcon == 'youtube') {
      return youtube.src;
    }
  };

  return (
    <Stack className='social-media-card' sx={{ margin: '0px 0 40px 0' }}>
      <div className='card-head'>
        <div className='card-user-data'>
          <img
            className='media-logo'
            src={bayada.src}
            alt='user profile picture'
          />
          <span className='card-user-name'>{data?.userName}</span>
        </div>
        <div className='social-icons'>
          <a href='#'>
            <img
              src={getSocialMediaIcon()}
              alt='social media icon'
              className='media-logo'
              width={28}
              height={28}
            />
          </a>
        </div>
      </div>
      <div className='card-media'>
        <img src={data.media} alt='social media post' />
        <button className='ba-play-btn'>
          <Icon iconName='play-btn' />
        </button>
      </div>
      <div className='card-bottom'>
        <span className='card-bottom-text'>5m ago</span>
        <div className='reactions-container'>
          <div className='reactions'>
            <Icon iconName='comment' />
            <span className='card-bottom-text'>{data.likesCount}</span>
          </div>
          <div className='reactions'>
            <Icon iconName='like' />
            <span className='card-bottom-text'>{data.commentscount}</span>
          </div>
          <div className='reactions'>
            <Icon iconName='forward' className='icon-forward' />
            <span className='card-bottom-text'>{data.sharesCount}</span>
          </div>
        </div>
      </div>
    </Stack>
  );
}

export default SocialMediaCard;
