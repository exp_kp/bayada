/* eslint-disable @typescript-eslint/no-explicit-any */
'use client';
import React from 'react';
import { DarkAccordion } from '@/app/(pages)/ui-components/accordion/accordion';
import './contentSection.css';
import { Container, Grid } from '@mui/material';
import { useContentfulInspectorMode } from '@contentful/live-preview/react';
import ContentfulImage from '@/app/components/common/ContentfulImage';

const ContentSection = (items: any) => {
  const inspectorProps = useContentfulInspectorMode();
  const { accordion, content } = items?.homeCareData ?? {};
  console.log(items, 'items accc');

  return (
    <Container fixed className='content-section-container'>
      <div className='content-section-grid flex flex-col md:flex-row'>
        <div className='w-full md:w-1/2'>
          <h2 className='t-25-36 mb-4 font-frutiger font-bold'>
            {content?.fields?.TextGroup?.fields.title}
          </h2>
          {content && content?.fields?.TextGroup && (
            <h2 className='t-21-18 mb-4 font-frutiger font-bold'>
              {content?.fields?.TextGroup?.fields.description}
            </h2>
          )}

          {content && content?.fields?.TextGroup && (
            <h2 className='t-12 mb-4 font-frutiger font-normal'>
              {content?.fields?.TextGroup?.fields.paragraph}
            </h2>
          )}
          {content && content?.fields?.Media && (
            <ContentfulImage
              className='homeCare-image'
              src={content?.fields?.Media?.fields?.file?.url}
              alt={'Care'}
              width={
                content?.fields?.Media?.fields?.file?.details?.image?.width
              }
              height={
                content?.fields?.Media?.fields?.file?.details?.image?.height
              }
            />
          )}
        </div>
        <div className='content-section-grid-accordion w-full md:w-1/2'>
          <div
            {...inspectorProps({
              entryId: accordion?.sys?.id,
              fieldId: 'home accordion',
            })}
          >
            {accordion &&
              accordion?.fields?.panels &&
              accordion?.fields?.panels.map((da: any, index: number) => (
                <div key={index} className='content-section-accordion-list'>
                  <DarkAccordion accordionData={da.fields} />
                </div>
              ))}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default ContentSection;
