/* eslint-disable @typescript-eslint/no-explicit-any */
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import {
  Button,
  Container,
  Divider,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@mui/material';

const FindCare = (props: any) => {
  const { findCare } = props;
  const findCareData = findCare;
  const dropDownItems = findCare?.dropDownItems?.fields;

  return (
    <div className='blue-gradient flex flex-col items-center justify-center'>
      <Container fixed className='flex flex-col items-center justify-center'>
        <div className='content-wrap xxl:w-1/2 mx-auto flex w-full flex-col items-center justify-center xl:w-3/4'>
          <h2 className='t-22-28 mb-4 mt-10 font-frutiger font-bold'>
            {findCareData?.title}
          </h2>

          <div className='input-group-cta  mb-16 flex w-full flex-col items-center justify-center rounded-md bg-white px-4 py-2 md:flex-row '>
            <FormControl
              fullWidth
              variant='standard'
              className='app-search-form-control grow '
              sx={{
                minWidth: '100px',
                marginBottom: 0,
                maxWidth: { xs: '100%', md: '40%' },
              }}
            >
              <InputLabel
                id='demo-simple-select-label'
                className='t-13 t-14-15 search-input-label mb-0 mt-1 !font-normal'
              >
                {findCareData?.dropDownItems?.fields?.name}
              </InputLabel>
              <Select
                className='app-search-form-select'
                defaultValue={dropDownItems?.fields?.menuItems[0].fields.id}
              >
                {dropDownItems?.menuItems?.map((items: any, index: number) => (
                  <MenuItem key={index} value={items.fields?.id}>
                    <h2 className='t-13'>{items.fields.name}</h2>
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <Divider
              sx={{
                height: 48,
                m: 0.5,
                marginRight: 2,
                marginLeft: 2,
                display: {
                  xs: 'none',
                  sm: 'block',
                },
              }}
              orientation='vertical'
            />

            <div className='flex grow flex-col justify-start'>
              <h2 className='t-13 mt-2 font-semibold'>
                {findCareData?.textGroup?.fields?.title}
              </h2>
              <h2 className='t-13 mb-2'>
                {findCareData?.textGroup?.fields?.description}
              </h2>
            </div>

            <Button
              variant='contained'
              color='primary'
              disableFocusRipple
              className='icon-btn-movement grow rounded-md md:ml-1'
              sx={{ marginBottom: 0 }}
              endIcon={
                <Icon
                  iconName='rightArrow'
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                ></Icon>
              }
            >
              <span className='t-15 font-inter font-medium leading-snug'>
                {findCareData?.button?.fields?.name}
              </span>
            </Button>
          </div>
        </div>
      </Container>
    </div>
  );
};
export default FindCare;
