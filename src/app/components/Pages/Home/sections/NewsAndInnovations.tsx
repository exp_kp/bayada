import Icon from '@/app/(pages)/ui-components/icon/Icon';
import './NewsAndInnovations.scss';
import { useEffect, useRef, useState } from 'react';
import { Box, Stack, Container } from '@mui/material';
import ContentfulImage from '@/app/components/common/ContentfulImage';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/scrollbar';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

// import required modules
import { Keyboard, Scrollbar, Navigation, Pagination } from 'swiper/modules';

export const NewsAndInnovations = (props: {
  data: {
    fields: {
      nICards: { sys: { id: string; linkType: string; type: string } }[];
      content: { sys: { id: string; linkType: string; type: string } };
    };
  };
}) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  // const carouselRef: MutableRefObject<any> = useRef(null);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [newsAndInnovations, setNewsAndInnovations] = useState<any[]>([]);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [nAndIContent, setnAndIContent] = useState<any[]>([]);

  // const [wrapperWidth, setWrapperWidth] = useState(0);
  const [csMargin, setCsMargin] = useState(0);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const sliderRef = useRef<HTMLDivElement | any>();

  const updateWrapperWidth = () => {
    const wrapperElement = sliderRef?.current;
    const scrollElement = wrapperElement?.querySelector('.swiper-scrollbar');
    if (wrapperElement && scrollElement) {
      const width = wrapperElement?.getBoundingClientRect().width;

      // setWrapperWidth(width);
      const customSpace = (window.innerWidth - width) / 2 - 9;
      setCsMargin(customSpace);
      const scrollStyle = {
        width: `calc(100% - ${customSpace}px)`,
        left: 0,
      };

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      Object.assign((scrollElement as any).style, scrollStyle);
    }
  };

  useEffect(() => {
    setNewsAndInnovations(props?.data?.fields?.nICards);
    setnAndIContent(props?.data?.fields?.content);

    updateWrapperWidth();
    const handleResize = () => {
      updateWrapperWidth();
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [props]);

  return (
    <Box
      sx={{
        padding: { xl: '100px 0', lg: '80px 0', md: '60px 0', xs: '48px 0' },
      }}
    >
      <Container fixed className='relative mx-auto'>
        <div className='ba-section-head'>
          <h2>{nAndIContent?.fields?.title}</h2>
          <p>{nAndIContent?.fields?.description}</p>
          <a href='#' className='btn-link'>
            Read all news
            <Icon
              iconName='rightArrow'
              className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
            ></Icon>
          </a>
        </div>
        <div
          className='ba-slider-wrapper'
          ref={sliderRef}
          style={{ marginRight: -csMargin }}
        >
          <Swiper
            slidesPerView={3.5}
            centeredSlides={false}
            slidesPerGroupSkip={3}
            grabCursor={true}
            keyboard={{
              enabled: true,
            }}
            breakpoints={{
              1200: {
                slidesPerView: 3.8,
                slidesPerGroup: 1,
              },
              991: {
                slidesPerView: 2.5,
                slidesPerGroup: 1,
              },
              769: {
                slidesPerView: 2.5,
                slidesPerGroup: 1,
              },
              575: {
                slidesPerView: 2,
                slidesPerGroup: 1,
              },
              0: {
                slidesPerView: 1.2,
                slidesPerGroup: 1,
              },
            }}
            scrollbar={true}
            navigation={false}
            pagination={false}
            modules={[Keyboard, Scrollbar, Navigation, Pagination]}
            className='mySwiper'
            spaceBetween={40}
          >
            {newsAndInnovations?.map((item, index) => (
              <SwiperSlide key={index}>
                <Stack
                  className='ba-slider-cards'
                  direction='column'
                  spacing={2}
                >
                  <Box className='ba-slider-cards-pic'>
                    <ContentfulImage
                      src={item?.fields?.Media?.fields?.file?.url}
                      alt={item?.fields?.Media?.fields?.title}
                      width={
                        item?.fields?.Media?.fields?.file?.details?.image?.width
                      }
                      height={
                        item?.fields?.Media?.fields?.file?.details?.image
                          ?.height
                      }
                      className='n-i-image-card'
                    />
                  </Box>
                  <Box className='ba-slider-cards-content'>
                    <h4>{item?.fields?.TextGroup?.fields?.title}</h4>
                    <p>{item?.fields?.TextGroup?.fields?.description}</p>
                    <a href='#' className='btn-link'>
                      {item?.fields?.button?.fields?.name}
                      <Icon
                        iconName='rightArrow'
                        className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                      ></Icon>
                    </a>
                  </Box>
                </Stack>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
      </Container>
    </Box>
  );
};
