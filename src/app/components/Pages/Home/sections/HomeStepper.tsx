import React from 'react';
import Image from 'next/image';
import { Box, Button, Container, Stack } from '@mui/material';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import './HomeStepper.scss';

import Typography from '@mui/material/Typography';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import { Pagination, Navigation } from 'swiper/modules';

const clientData = [
  {
    tagText: 'Personal Impact1',
    mainTitle: 'Our Clients are our purpose',
    mainDescription: `In the heart of our care lies the bond between client and caregiver. Together, we craft a journey of trust, comfort, and empowerment. With compassion, excellence, and reliability as our compass, we navigate challenges, celebrate victories, and build a foundation of well-being.`,
    cardItem: [
      {
        itemTitle: 'Unwavering access to care1',
        itemDescription:
          'We understand the importance of consistently meeting and exceeding expectations throughout our the relationship with our clients. We are deeply committed to providing our caregivers with robust support systems, enhanced stability, and exceptional educational and career advancement opportunities.',
      },
      {
        itemTitle: 'Holistic healing through elevated quality',
        itemDescription:
          'BAYADA enables holistic healing through proactively identifying and closing care gaps. Being a non-for-profit allows us to continuously invest in talent, technology, and care delivery, ensuing enduring high quality care without being constrained by the financial demands of stakeholders.',
      },
    ],
    stepperImage: '/temp/client-pic1.png',
  },
  {
    tagText: 'Personal Impact',
    mainTitle: 'Our Clients are our purpose',
    mainDescription: `In the heart of our care lies the bond between client and caregiver. Together, we craft a journey of trust, comfort, and empowerment. With compassion, excellence, and reliability as our compass, we navigate challenges, celebrate victories, and build a foundation of well-being.`,
    cardItem: [
      {
        itemTitle: 'Unwavering access to care',
        itemDescription:
          'We understand the importance of consistently meeting and exceeding expectations throughout our the relationship with our clients. We are deeply committed to providing our caregivers with robust support systems, enhanced stability, and exceptional educational and career advancement opportunities.',
      },
      {
        itemTitle: '2',
        itemDescription:
          'We understand the importance of consistently meeting and exceeding expectations throughout our the relationship with our clients. We are deeply committed to providing our caregivers with robust support systems, enhanced stability, and exceptional educational and career advancement opportunities.',
      },
    ],
    stepperImage: '/temp/client-pic1.png',
  },
];

export const HomeStepper = () => {
  return (
    <Stack
      spacing={{ xs: 1, sm: 2, md: 10, lg: 15 }}
      direction='row'
      useFlexGap
    >
      <Swiper
        pagination={{
          type: 'fraction',
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className='mySwiper'
      >
        {clientData.map((mainItem, mainIndex) => {
          return (
            <SwiperSlide key={mainIndex}>
              <Container fixed className='relative mx-auto my-8 py-6'>
                <Box
                  className='my-6 flex w-full flex-col items-start justify-end'
                  sx={{
                    paddingRight: {
                      sm: '0',
                      md: '300px',
                      lg: '350px',
                      xl: '500px',
                    },
                  }}
                >
                  <span className='ba-tag primary t-12 mb-1 font-frutiger font-normal uppercase'>
                    {mainItem.tagText}
                  </span>
                  <h3 className='t-33-46 !font-frutiger !font-bold leading-[3rem] md:mt-4'>
                    {mainItem.mainTitle}
                  </h3>
                  <p className='t-16-17 mt-5'>{mainItem.mainDescription}</p>
                  <Box
                    className='client-stepper-img mt-1 sm:mt-1'
                    sx={{
                      display: { sm: 'block', md: 'none' },
                      width: { xs: '227px', sm: '400px' },
                      height: { xs: '227px', sm: '400px' },
                      position: { xs: 'relative', sm: 'relative' },
                      margin: { xs: '10px auto', sm: 'auto' },
                    }}
                  >
                    <figure>
                      <img src={mainItem.stepperImage} alt='background' />
                    </figure>
                  </Box>
                  <Stack
                    spacing={{ xs: 1, sm: 2, md: 5, lg: 6 }}
                    direction='row'
                    useFlexGap
                    sx={{
                      mt: 5,
                      display: { xs: 'none', sm: 'none', md: 'flex' },
                      width: '100%',
                    }}
                  >
                    {mainItem.cardItem.map((item, index) => (
                      <Box
                        key={index}
                        sx={{
                          width: '50%',
                          boxShadow: 'none',
                          background: 'none',
                        }}
                      >
                        <Typography
                          sx={{ mb: 1.5, maxWidth: { xl: '310px' } }}
                          variant='h5'
                          color='text.primary'
                          className='t-33-28 !font-frutiger !font-bold'
                        >
                          {item.itemTitle}
                        </Typography>
                        <Typography variant='body2' color='text.secondary'>
                          {item.itemDescription}
                        </Typography>
                      </Box>
                    ))}
                  </Stack>
                  <Stack
                    spacing={{ xs: 1, sm: 2, md: 10, lg: 15 }}
                    direction='row'
                    justifyContent={'space-between'}
                    useFlexGap
                    sx={{ marginTop: 5, width: '100%' }}
                  >
                    <Button
                      variant='outlined'
                      color='secondary'
                      disableFocusRipple
                      className='icon-btn-movement'
                      endIcon={
                        <Icon
                          iconName='rightArrow'
                          className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                        ></Icon>
                      }
                    >
                      <span className='t-15 font-inter font-medium leading-snug'>
                        Get care
                      </span>
                    </Button>
                  </Stack>
                </Box>
              </Container>
              <Box
                className='client-stepper-image'
                sx={{
                  mt: 2,
                  display: { xs: 'none', sm: 'none', md: 'block' },
                  width: { md: '500px', lg: '500px', xl: '600px' },
                  height: { md: '500px', lg: '500px', xl: '600px' },
                }}
              >
                <figure>
                  <Image
                    src={mainItem.stepperImage}
                    width={600}
                    height={600}
                    alt='background'
                  />
                </figure>
              </Box>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </Stack>
  );
};
