/* eslint-disable @typescript-eslint/no-explicit-any */
import './LocationBlock.scss';
import React from 'react';
import { TextField } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';

// import ContentfulImage from '@/app/components/contentfulImage';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import ContentfulImage from '@/app/components/common/ContentfulImage';

const LocationBlock = (props: any) => {
  console.log('description', props?.response?.Media.fields.file.url);

  const locationBlockDetails = {
    title: props?.response?.TextGroup?.fields?.title,
    description: props?.response?.TextGroup?.fields?.description,
    imageUrl: props?.response?.Media.fields.file.url,
    imageAltText: props?.response?.Media.fields.file.title,
    imageWidth: props?.response?.Media.fields.file.details.image.width,
    imageHeight: props?.response?.Media.fields.file.details.image.height,
    searchPlaceholder: props?.response?.search?.fields?.placeholder,
  };

  return (
    <div className='jobs-home-container container mx-auto max-w-full justify-center md:flex'>
      <div
        id='title-description-container'
        className='flex min-h-[390px] px-9 md:w-full'
      >
        <div className='flex flex-col justify-center md:items-start md:px-20'>
          <h1 className='t-46-64 mb-7 font-inter font-bold md:max-w-[600px]'>
            {`BAYADA ${locationBlockDetails.title}`}
          </h1>
          <span className='t-22-28 mb-7 font-frutiger font-bold'>
            {locationBlockDetails.description}
          </span>
          <TextField
            id='input-with-icon-textfield'
            type='search'
            defaultValue=''
            placeholder={locationBlockDetails.searchPlaceholder}
            variant='outlined'
            className='xs:sm:self-center md:max-w-sm md:self-stretch'
            sx={{
              marginBottom: 0,
              borderRadius: '20px',
              outline: 'none !important',
              border: 'none',
            }}
            InputProps={{
              startAdornment: (
                <InputAdornment position='start'>
                  <Icon
                    iconName='search'
                    className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                  ></Icon>
                </InputAdornment>
              ),
            }}
          />
        </div>
      </div>
      <div id='map-container' className='xs:min-h-[390px] w-full'>
        <ContentfulImage
          src={locationBlockDetails.imageUrl}
          alt={locationBlockDetails.imageAltText}
          width={locationBlockDetails.imageWidth}
          height={locationBlockDetails.imageHeight}
          object='fill'
        />
      </div>
    </div>
  );
};

export default LocationBlock;
