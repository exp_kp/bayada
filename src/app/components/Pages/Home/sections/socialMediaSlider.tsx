'use client';

import React, { useEffect, useRef, useState } from 'react';
import json from './dummy.json';
import Card from './SocialMediaCard';
import { Box, Container } from '@mui/material';
import { Swiper, SwiperSlide } from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/scrollbar';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
// import required modules
import { Keyboard, Scrollbar, Navigation, Pagination } from 'swiper/modules';
import './socialmediaSlider.scss';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function SocialMediaSlider(props: any) {
  const { fields } = props;
  const [wrapperWidth, setWrapperWidth] = useState(0);
  const [csMargin, setCsMargin] = useState(0);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const sliderRef = useRef<HTMLDivElement | any>();

  const updateWrapperWidth = () => {
    const wrapperElement = sliderRef?.current;
    const scrollElement = wrapperElement?.querySelector('.swiper-scrollbar');
    if (wrapperElement && scrollElement) {
      const width = wrapperElement.getBoundingClientRect().width;
      setWrapperWidth(width);
      const customSpace = (window.innerWidth - width) / 2 - 9;
      setCsMargin(customSpace);
      const scrollStyle = {
        width: `calc(100% - ${customSpace}px)`,
        left: 0,
      };

      Object.assign(scrollElement.style, scrollStyle);
    }
  };
  useEffect(() => {
    updateWrapperWidth();
    const handleResize = () => {
      updateWrapperWidth();
    };
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [props]);

  return (
    <Box sx={{ py: 8 }}>
      <Container fixed className='social-media-container relative mx-auto'>
        {fields?.fields?.TextGroup?.fields?.description && (
          <div className='ba-section-head'>
            <h2>{fields?.fields?.TextGroup?.fields?.title}</h2>
            <p>{fields?.fields?.TextGroup?.fields?.description}</p>
          </div>
        )}

        <div
          className='ba-slider-wrapper'
          ref={sliderRef}
          style={{ marginRight: -csMargin }}
        >
          <Swiper
            slidesPerView={3.5}
            centeredSlides={false}
            slidesPerGroupSkip={3}
            grabCursor={true}
            keyboard={{
              enabled: true,
            }}
            breakpoints={{
              1200: {
                slidesPerView: 3.3,
                slidesPerGroup: 1,
              },
              991: {
                slidesPerView: 2.5,
                slidesPerGroup: 1,
              },
              769: {
                slidesPerView: 2.5,
                slidesPerGroup: 1,
              },
              575: {
                slidesPerView: 2,
                slidesPerGroup: 1,
              },
              0: {
                slidesPerView: 1.2,
                slidesPerGroup: 1,
              },
            }}
            scrollbar={true}
            navigation={false}
            pagination={false}
            modules={[Keyboard, Scrollbar, Navigation, Pagination]}
            className='mySwiper'
            spaceBetween={40}
          >
            {
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              json.map((item: any, index: number) => {
                return (
                  <SwiperSlide key={index}>
                    <Card data={item} />
                  </SwiperSlide>
                );
              })
            }
          </Swiper>
        </div>
      </Container>
    </Box>
  );
}

export default SocialMediaSlider;
