/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import HeroSection from '../HeroBanner/HeroSection';
import ContentSection from '../../Pages/Home/sections/contentSection';
import FindCare from '../../Pages/Home/sections/findCare';
import ChainOfImpact from '../../Pages/Home/sections/ChainOfImpact';
import { NewsAndInnovations } from '../../Pages/Home/sections/NewsAndInnovations';
import LocationBlock from '../../Pages/Home/sections/LocationBlock';
import SocialMediaSlider from '../../Pages/Home/sections/socialMediaSlider';
import ListOfFacts from '../../Pages/Home/sections/ListOfFacts';
import { useContentfulInspectorMode } from '@contentful/live-preview/react';

type Section = {
  data?: any;
};

const Section: React.FC<Section> = ({ data }) => {
  console.log(data, `${data?.fields?.internalName}:: data`);
  const inspectorProps = useContentfulInspectorMode();
  const props = {
    ...inspectorProps({
      entryId: data?.sys?.id,
      fieldId: data?.fields?.internalName ?? data?.fields?.name,
    }),
  };
  switch (data?.fields?.internalName ?? data?.fields?.name) {
    case 'Home Hero Section':
      return (
        <section {...props}>
          <HeroSection className='' data={data?.fields} />
        </section>
      );
    case 'Home Care Section':
      return (
        <section {...props}>
          <ContentSection homeCareData={data?.fields} />
        </section>
      );
    case 'Find the care you need':
      return (
        <section {...props}>
          <FindCare findCare={data?.fields} />
        </section>
      );
    case 'List of facts':
      return (
        <section {...props}>
          <ListOfFacts list={data?.fields} />
        </section>
      );
    case 'Chain Of Impact section':
      return (
        <section {...props}>
          <ChainOfImpact response={data?.fields} />
        </section>
      );
    case 'News and innovation cards':
      return (
        <section {...props}>
          <NewsAndInnovations data={data} />
        </section>
      );
    case 'Locations Block':
      return (
        <section {...props}>
          <LocationBlock response={data?.fields} />
        </section>
      );
    case 'SocialMedia Block':
      return (
        <section {...props}>
          <SocialMediaSlider fields={data} />
        </section>
      );
    default:
      return <p>s</p>;
  }

  //   return <div>Section</div>;
};

export default Section;
