/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import Image from 'next/image';

const nextImageLoader = ({ src, width, quality }: any) => {
  return `https:${src}?w=${width}&q=${quality || 75}`;
};

const ContentfulImage = (props: any) => {
  console.log(props, 'ContentfulImage props');
  return (
    <Image
      alt={props?.alt ? props?.alt : 'image'}
      width={props?.width ? props?.width : 0}
      height={props?.height ? props?.height : 0}
      sizes='100vw'
      loader={nextImageLoader}
      style={{ width: 'auto', height: 'auto' }}
      {...props}
    />
  );
};

export default ContentfulImage;
