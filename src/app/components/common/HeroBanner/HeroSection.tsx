'use client';
import { Box, Button, Container, Grid } from '@mui/material';
import React, { useEffect } from 'react';
import ContentfulImage from '../ContentfulImage';
import './HeroSection.scss';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import { useContentfulInspectorMode } from '@contentful/live-preview/react';

export type HeroSection = {
  className: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
};

const HeroSection: React.FC<HeroSection> = ({ className, data }) => {
  console.log(data, 'data');
  const title = data?.TextGroup?.fields?.title;
  const description = data?.TextGroup?.fields?.description;
  const paragraph = data?.TextGroup?.fields?.paragraph;
  const image = {
    file: data?.Media?.fields?.file,
    title: data?.Media?.fields?.title,
    description: data?.Media?.fields?.contentType,
  };

  const inspectorProps = useContentfulInspectorMode();

  useEffect(() => {}, []);

  return (
    <>
      <Box className={className}>
        <div className={'hero-banner'}>
          <ContentfulImage
            src={image?.file?.url}
            alt={image?.title}
            quality='100'
            width={image?.file?.details?.image?.width}
            height={image?.file?.details?.image?.height}
          />
          <Container fixed className={'hero-banner-content'} sx={{ py: 16 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={8} lg={6} xl={6}>
                <h1
                  className='t-46-64 mb-8 !font-frutiger !font-bold leading-[5rem] text-white md:mt-6'
                  {...inspectorProps({
                    entryId: data?.TextGroup?.sys?.id,
                    fieldId: 'title',
                  })}
                >
                  {title}
                </h1>
                <h2
                  className='t-21-18 mb-3 !font-frutiger !font-bold text-white'
                  {...inspectorProps({
                    entryId: data?.TextGroup?.sys?.id,
                    fieldId: 'description',
                  })}
                >
                  {description}
                </h2>
                <h2
                  className='t-16-17 my-2 !font-frutiger  text-white'
                  {...inspectorProps({
                    entryId: data?.TextGroup?.sys?.id,
                    fieldId: 'paragraph',
                  })}
                >
                  {paragraph}
                </h2>
                <Button
                  variant='text'
                  sx={{
                    borderRadius: '4px',
                    maxWidth: '402px',
                    width: '100%',
                  }}
                  className='hero-search !mr-auto !mt-10 flex justify-center bg-white'
                  startIcon={
                    <Icon
                      iconName='search'
                      className='svg-icon icon-24 flex items-center justify-center'
                    ></Icon>
                  }
                  color='secondary'
                  disableFocusRipple
                >
                  <span
                    className='t-14 text-cta my-2 !font-frutiger !font-semibold normal-case'
                    {...inspectorProps({
                      entryId: data?.search?.sys?.id,
                      fieldId: 'placeholder',
                    })}
                  >
                    {data?.search?.fields?.placeholder}
                  </span>
                </Button>
              </Grid>
              <Grid item xs={6} lg={6} md={8}></Grid>
            </Grid>
          </Container>
        </div>
      </Box>
    </>
  );
};

export default HeroSection;
