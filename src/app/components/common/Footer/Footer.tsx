/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @next/next/no-img-element */
'use client';

import { Button, Container, TextField } from '@mui/material';
import './footer.scss';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import React, { useState } from 'react';
import ContentfulImage from '@/app/components/common/ContentfulImage';
import { Entry, EntrySkeletonType } from 'contentful';
import {
  useContentfulInspectorMode,
  useContentfulLiveUpdates,
} from '@contentful/live-preview/react';

type Footer = {
  data: Entry<EntrySkeletonType, undefined, string> | undefined;
};

const Footer: React.FC<Footer> = ({ data }) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [footerContent] = useState<
    Entry<EntrySkeletonType, undefined, string> | undefined
  >(data);
  const inspectorProps = useContentfulInspectorMode();
  const updatedState = useContentfulLiveUpdates(data);
  console.log(data, ':::footer rendered');

  return (
    <div
      className='footer-container'
      {...inspectorProps({
        entryId: (footerContent as any)?.sys?.id,
        fieldId: (footerContent as any)?.fields?.internalName,
      })}
    >
      <Container fixed className='relative mx-auto'>
        <div className='foot-main'>
          <div className='footer-left'>
            <div className='footer-group'>
              <img
                className='logo'
                // src={updatedState?.fields?.logo.fields.image.fields.asset.fields.file.url}
                src='/temp/bayada-color-logo.png'
                alt='Bayada Logo'
              />

              <div className='footer-icons'>
                {// eslint-disable-next-line @typescript-eslint/no-explicit-any
                (updatedState?.fields?.linkGroup as Array<any>)?.map(
                  (mediaImg: any) => (
                    <ContentfulImage
                      key={mediaImg.sys.id}
                      src={mediaImg.fields.image.fields.asset.fields.file.url}
                      alt={mediaImg.fields?.internalName}
                    />
                  )
                )}
              </div>
            </div>

            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            (updatedState?.fields?.textGroup as Array<any>)?.map(
              (text: any) => (
                <h2
                  key={text.sys.id}
                  className='t-21-18 mb-4 w-60 font-frutiger font-bold'
                >
                  {text.fields.title}
                </h2>
              )
            )}
            <div className='input-group-cta mb-4 flex items-center bg-white px-2 py-1'>
              <TextField
                required
                id='outlined-required'
                label='Enter Email Adress'
                defaultValue=''
                variant='filled'
                className='filled-borderless-field search'
                sx={{ marginBottom: 0 }}
              />
              <Button
                variant='contained'
                color='secondary'
                disableFocusRipple
                className='icon-btn-movement md:ml-1'
                sx={{ marginBottom: 0, marginLeft: 1 }}
              >
                <Icon
                  iconName='rightArrow'
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                ></Icon>
              </Button>
            </div>
          </div>
          <div className='footer-service'>
            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            (updatedState?.fields?.footerMenuGroup as Array<any>)?.map(
              (menuItem: any) =>
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                menuItem.fields.menuItems.map((data: any, index: number) => (
                  <div key={index} className='service-links'>
                    <h2 className='t-14-15 mb-4 font-semibold'>
                      {data.fields.name}
                    </h2>
                    <ul>
                      {
                        // eslint-disable-next-line @typescript-eslint/no-explicit-any
                        data.fields.menuItems.map(
                          (listItem: any, i: number) => (
                            <li key={i}>
                              <a href='#'>{listItem.fields.name}</a>
                            </li>
                          )
                        )
                      }
                    </ul>
                  </div>
                ))
            )}
          </div>
          <div className='footer-group'>
            <img
              className='logo'
              // src={updatedState?.fields?.logo.fields.image.fields.asset.fields.file.url}
              src='/temp/bayada-color-logo.png'
              alt='Bayada Logo'
            />

            <div className='footer-icons'>
              {// eslint-disable-next-line @typescript-eslint/no-explicit-any
              (updatedState?.fields?.linkGroup as Array<any>)?.map(
                (mediaImg: any) => (
                  <ContentfulImage
                    width={32}
                    height={32}
                    key={mediaImg.sys.id}
                    src={mediaImg.fields.image.fields.asset.fields.file.url}
                    alt={mediaImg.fields.internalName}
                  />
                )
              )}
            </div>
          </div>
        </div>
      </Container>

      <div className='footer-bottom'>
        <Container fixed className='relative mx-auto'>
          <div className='footer-bottom-content'>
            <img src='/temp/chap-logo.png' alt='chap' />
            {// eslint-disable-next-line @typescript-eslint/no-explicit-any
            (updatedState?.fields?.textGroup as Array<any>)?.map(
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              (text: any, index: number) => (
                <p key={index}>{text.fields.description}</p>
              )
            )}
          </div>
        </Container>
      </div>
    </div>
  );
};

export default Footer;
