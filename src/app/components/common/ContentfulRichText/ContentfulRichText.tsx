/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { documentToReactComponents } from '@contentful/rich-text-react-renderer';

import { BLOCKS } from '@contentful/rich-text-types';

const options = {
  renderNode: {
    [BLOCKS.EMBEDDED_ASSET]: (node: any) => {
      // eslint-disable-next-line jsx-a11y/alt-text
      return <img />;
    },
    [BLOCKS.HEADING_1]: (node: any, children: any) => {
      return (
        <h1
          style={{
            padding: '0',
            color: 'red',
            margin: '0',
            display: 'inline-block',
          }}
        >
          {children}
        </h1>
      );
    },
    [BLOCKS.HEADING_2]: (node: any, children: any) => {
      return (
        <h1
          style={{
            padding: '0',
            color: 'red',
            margin: '0',
            display: 'inline-block',
          }}
        >
          {children}
        </h1>
      );
    },
    [BLOCKS.HEADING_3]: (node: any, children: any) => {
      return (
        <h1
          style={{
            padding: '0',
            color: 'red',
            margin: '0',
            display: 'inline-block',
          }}
        >
          {children}
        </h1>
      );
    },
    [BLOCKS.HEADING_4]: (node: any, children: any) => {
      return (
        <h1
          style={{
            padding: '0',
            color: 'red',
            margin: '0',
            display: 'inline-block',
          }}
        >
          {children}
        </h1>
      );
    },
    [BLOCKS.HEADING_5]: (node: any, children: any) => {
      return <h5>{children}</h5>;
    },
    [BLOCKS.PARAGRAPH]: (node: any, children: any) => {
      return <p className='t-14 font-frutiger font-bold'>{children}</p>;
    },
    [BLOCKS.LIST_ITEM]: (node: any, children: any) => {
      return (
        <li
          style={{
            padding: '0',
            color: 'yellow',
            margin: '0',
            display: 'inline-block',
          }}
        >
          {children}
        </li>
      );
    },
  },
};

const ContentfulRichText = (richTextField: any) => {
  console.log('richTextField?.richtext', richTextField?.richtext);

  return (
    <div>{documentToReactComponents(richTextField?.richtext, options)}</div>
  );
};

export default ContentfulRichText;
