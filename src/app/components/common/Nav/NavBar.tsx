/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable prettier/prettier */
'use client';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import Container from '@mui/material/Container';
import useScrollDirection from '../../../hooks/scrollDirection';
import './NavBar.scss';
import { Entry, EntrySkeletonType } from 'contentful';
import Icon from '@/app/(pages)/ui-components/icon/Icon';
import ContentfulImage from '../ContentfulImage';
import ContentfulRichText from '../ContentfulRichText/ContentfulRichText';
import {
  useContentfulInspectorMode,
  useContentfulLiveUpdates,
} from '@contentful/live-preview/react';

type NavBar = {
  data: Entry<EntrySkeletonType, undefined, string> | undefined;
};

const NavBar: React.FC<NavBar> = ({ data }) => {
  const pathname = usePathname();
  const [state, setState] = useState<
    Entry<EntrySkeletonType, undefined, string> | undefined
  >(data);
  const updatedState = useContentfulLiveUpdates(state);
  const inspectorProps = useContentfulInspectorMode();
  const [scrollWatchEnabled, setscrollWatchEnabled] = useState(false);
  const scrollDirection = useScrollDirection();
  // console.log(scrollDirection, 'scrollDirection');

  useEffect(() => {
    const handleScroll = () => {
      if (!scrollWatchEnabled) {
        setscrollWatchEnabled(true);
      }
    };
    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  console.log(state, 'state ss');

  const { menuItemGroupContainer } = updatedState?.fields ?? {};
  const [topBarLeft, topbarRight, menuLeft, menuRight] =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (menuItemGroupContainer as Array<any>) ?? [];
  const [logoItem] = (menuLeft?.fields?.menuItems as Array<any>) ?? [];

  return (
    <>
      <div
        className={`app-nav-wrap ${
          scrollDirection?.scrollDirection === 'up' &&
          scrollDirection?.scrollY > 150
            ? ''
            : 'has-transparent-nav'
        } fixed top-0 w-screen`}
        style={{
          transform: `translateY(${
            scrollDirection?.scrollDirection === 'up'
              ? 0
              : scrollWatchEnabled
              ? '-100%'
              : 0
          })`,
        }}
      >
        <div className='top-bar flex items-center bg-white'>
          <Container
            fixed
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <div
              className='top-bar-left'
              {...inspectorProps({
                entryId: topBarLeft?.sys?.id,
                fieldId: 'Header Left',
              })}
            >
              <ul className='navigation-list flex items-center'>
                {topBarLeft?.fields?.menuItems?.map((item: any) => {
                  return (
                    <li
                      key={item?.fields?.internalName}
                      className='nav-item mr-2 flex items-center'
                    >
                      {item?.fields?.icon && (
                        <span className='menu-icon mr-2'>
                          <Icon
                            iconName={item?.fields?.icon?.fields?.icons}
                            className='svg-icon icon-16'
                          />
                        </span>
                      )}
                      {item?.fields?.name !== 'Search' && (
                        <span className='t-14 font-frutiger font-bold leading-none'>
                          {item?.fields?.name}
                        </span>
                      )}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div
              className='top-bar-right'
              {...inspectorProps({
                entryId: topbarRight?.sys?.id,
                fieldId: 'Header Right',
              })}
            >
              <ContentfulRichText richtext={topbarRight?.fields?.textContent} />
            </div>
          </Container>
        </div>
        <div className='primary-navbar flex items-center'>
          <Container
            fixed
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <div
              className='primary-nav-logo'
              {...inspectorProps({
                entryId: logoItem?.sys?.id,
                fieldId: 'logo',
              })}
            >
              {logoItem && (
                <Link href='/' className='nav-link'>
                  <ContentfulImage
                    src={logoItem?.fields?.image?.fields?.file?.url}
                    alt={logoItem?.fields?.image?.fields?.title}
                    width={193}
                    height={61}
                  />
                </Link>
              )}
            </div>
            <div className='navigation-group'>
              <ul
                className='navigation-list item-center flex'
                {...inspectorProps({
                  entryId: menuRight?.sys?.id,
                  fieldId: 'Header Bottom Right',
                })}
              >
                {menuRight?.fields?.menuItems?.map((item: any) => {
                  return (
                    <li
                      key={item?.fields?.internalName}
                      className='nav-item ml-2'
                    >
                      <Link href='/get-a-care' className='nav-link'>
                        <span className='t-16-17 font-frutiger font-semibold'>
                          {item?.fields?.name}
                        </span>
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </div>
          </Container>
        </div>
      </div>
    </>
  );
};

export default NavBar;
