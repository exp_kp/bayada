'use client';
import React from 'react';
import { ContentfulLivePreviewProvider } from '@contentful/live-preview/react';
import { useSearchParams } from 'next/navigation';

type PageContainer = {
  children: React.ReactNode;
};

const PageContainer: React.FC<PageContainer> = ({ children }) => {
  const searchParams = useSearchParams();
  const search = searchParams.get('preview');
  const preview = search ? true : false;

  return (
    <div className='PageContainer'>
      <ContentfulLivePreviewProvider
        locale='en-US'
        enableLiveUpdates={preview}
        enableInspectorMode={preview}
      >
        {children}
      </ContentfulLivePreviewProvider>
    </div>
  );
};

export default PageContainer;
