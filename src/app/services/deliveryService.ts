import { client } from '../config/client';

export const getEntries = async (query: { [key: string]: unknown }) => {
  try {
    const res = await client?.getEntries({ ...query, include: 10 });
    console.log(res, '::: getEntries response');

    return res;
  } catch (error) {
    console.log(error, '::: getEntries error');
  }
};
